package com.vigion.sql;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class ActivityLogin extends AppCompatActivity {

    //objetos graficos
    EditText txtUserId, txtUserPass, txtLogin;
    Button btnLogin;
    ProgressBar pbbar;

    //comunicação entre Activities
    String sgbd = "";
    Intent intentObject = null;

    //connector SQL
    ConnectionSQL connectionSQL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        //Inicialização do Connector e aquisição dos objetos graficos
        connectionSQL = new ConnectionSQL();                            //connectior inicializado
        txtUserId = (EditText) findViewById(R.id.txtUserId);            //adquire controlo de txtUserId
        txtUserPass = (EditText) findViewById(R.id.txtUserPass);            //adquire controlo de txtUserPass
        btnLogin = (Button) findViewById(R.id.btnLogin);                //adquire controlo de botao
        pbbar = (ProgressBar) findViewById(R.id.pbbar);                 //adquire controlo de progressBar
        pbbar.setVisibility(View.GONE);                                 //Declara-a invisivel

        //comunicação entre activities
        intentObject = getIntent();                     //inicialização do Intent
        sgbd = intentObject.getStringExtra("sgbd");     //extração do dado
        //txtLogin.setText(txtLogin.getText() + " " + sgbd);  //atuaçização do titulo

        //Listener do botao executa o login
        btnLogin.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Login login = new Login();
                login.execute("");
            }
        });
    }

    //Classe interna para executar o login numa Thread Assincrona
    public class Login extends AsyncTask<String, String, String>{

        String msg = "";            //mensagem para o utilizador
        Boolean sucesso = false;    //controlo da execução
        String userId = txtUserId.getText().toString();
        String password = txtUserPass.getText().toString();

        @Override       //executa as açoes principais em background e independente da activity
        protected String doInBackground(String... params) {
            if (userId.trim().equals(" ") || password.trim().equals(" ")){
                msg = "Introduza credenciais para login";
            }
            else {
                try {       //tentativa de ligaçao à BD
                    Connection con = connectionSQL.connectionSQLServer();
                    if (sgbd == "SQLServer"){
                        con = connectionSQL.connectionSQLServer();
                    }
                    else if (sgbd == "MySql"){
                        con = connectionSQL.connectMySql();
                    }
                    //Connection con = connectionSQL.connectionSQLServer();

                    if (con == null){
                        msg = "Erro na ligaçao: " + sgbd;
                    }
                    else {
                        //ligação com sucesso, tenta executar o comando SQL DML
                        String query = "select * from Aluno where Username='" + userId +
                                "' and Password='" + password + "'";
                        Statement stmt = con.createStatement();//Executa a DML
                        ResultSet rs = stmt.executeQuery(query);//Recebe o resultado
                        if (rs.next()){                         //se ha registos devolvidos
                            msg = "Login bem sucedido";         //teve sucesso. encontrou user
                            sucesso = true;                     //
                        }else {                                 //nao encontrou o user
                            msg = "Credenciais invalidas";      //prepara mensagem
                            sucesso = false;                    //confirma sucesso a false
                        }
                    }
                } catch (Exception ex){
                    sucesso = false;
                    msg = "Execução: "+ex.getMessage();
                }
            }
            return msg;
        }

        @Override       //executa ações preparatorias, antes da doInBackground
        protected void onPostExecute(String s) {
            pbbar.setVisibility(View.GONE);
            Toast.makeText(ActivityLogin.this, s, Toast.LENGTH_SHORT).show();
            if (sucesso){
                //se ligaçao ok, chama a proxima activity para tratar dados: inserir, listar,
                //startActivity(new Intent(ActivityLogin.this, promixaActivity.class));
                finish();       //metodo desta classe para terminar a açao em background
            }
        }

        @Override
        protected void onPreExecute() {
            pbbar.setVisibility(View.VISIBLE);
        }
        //Metodos Override: obrigatorios implementar nesta classe
    }

}
