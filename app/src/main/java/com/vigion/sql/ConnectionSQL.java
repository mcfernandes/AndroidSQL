package com.vigion.sql;

import android.annotation.SuppressLint;
import android.os.StrictMode;
import android.test.suitebuilder.annotation.Suppress;
import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by a48991 on 07-01-2016.
 */
public class ConnectionSQL {
    //Atributos: Credenciais dos SGBDs e Base de Dados
    //SQL Server
    private String sqlServerDriver ="net.sourceforge.jtds.jdbc.Driver";     //Connector
    private String sqlServerlocation = "10.1.16.202";
    private String sqlServerDatabase = "Escola";
    private String sqlServerDbUserId = "tgpsi";
    private String sqlServerDbPassword = "esferreira123";

    //SGBD MYSQL
    private String mySqlDriver = "com.mysql.jdbc.Driver";       //Connetor  MySQL
    private String mySqlServerLocation = "10.1.1.183";      //ip ou url da maquina com o sgbd MySQL
    private String mySqlDatabase = "escola";
    private String mySqlDbUserId = "root";
    private String mySqlDbPassword = "";

    //Método devolve uma Connection Pronta a usar
    @SuppressLint("NewApi") //suprime varnnings desta classe
    public Connection connectionSQLServer(){
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Connection conn = null;
        String ConnURL = null;
        try {
            Class.forName(sqlServerDriver);
            ConnURL = "jdbc:jtds:sqlserver://" + sqlServerlocation
                    + ";databaseName=" + sqlServerDatabase
                    + ";user=" + sqlServerDbUserId
                    + ";password=" + sqlServerDbPassword + ";";
            conn = DriverManager.getConnection(ConnURL);
        } catch (SQLException ex){
            Log.e("ERRO", ex.getMessage());
        }catch (ClassNotFoundException ex){
            Log.e("ERRO", ex.getMessage());
        }
        catch (Exception ex){
            Log.e("ERRO", ex.getMessage());
        }
        return conn;
    }

    public Connection connectMySql(){
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Connection conn = null;
        String ConnURL = null;
        try {
            Class.forName(mySqlDriver);
            ConnURL = "jdbc:sqlserver://" + mySqlServerLocation
                    + ";databaseName=" + mySqlDatabase
                    + ";user=" + mySqlDbUserId
                    + ";password=" + mySqlDbPassword + ";";
            conn = DriverManager.getConnection(ConnURL);
        } catch (SQLException ex){
            Log.e("ERRO", ex.getMessage());
        } catch (ClassNotFoundException ex){
            Log.e("ERRO", ex.getMessage());
        } catch (Exception ex){
            Log.e("ERRO", ex.getMessage());
        }
        return conn;
    }
}