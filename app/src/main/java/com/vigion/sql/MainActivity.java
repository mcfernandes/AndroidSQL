package com.vigion.sql;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private RadioGroup radioGroup;
    private Button buttonLigaSql;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        buttonLigaSql = (Button) findViewById(R.id.button);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){

            @Override       //receb o radioGroup e o id do radioButton checked
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId){     //qual dos radiobuttons foi selecionado?
                    case R.id.radioButtonMySQL:
                        Toast.makeText(getApplicationContext(), "MySQL", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioButtonSQLServer:
                        Toast.makeText(getApplicationContext(), "SQLServer", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioButtonSQLite:
                        Toast.makeText(getApplicationContext(), "SQLite", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        Toast.makeText(getApplicationContext(), "ERRO", Toast.LENGTH_SHORT).show();
                }
            }
        });

        buttonLigaSql.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                int selectedId = radioGroup.getCheckedRadioButtonId();
                Intent intent = new Intent(getApplicationContext(), ActivityLogin.class);

                switch (selectedId){     //qual dos radiobuttons foi selecionado?
                    case R.id.radioButtonMySQL:
                        Toast.makeText(getApplicationContext(), "MySQL", Toast.LENGTH_SHORT).show();
                        intent.putExtra("sgbd", "MySQL");   //insere a string sgbd com "MySQL" no objeto
                        startActivity(intent);              //chama a Activity
                        break;

                    case R.id.radioButtonSQLServer:
                        Toast.makeText(getApplicationContext(), "SQLServer", Toast.LENGTH_SHORT).show();
                        intent.putExtra("sgbd", "SQLServer");   //insere a string sgbd com "SQLServer" no objeto
                        startActivity(intent);              //chama a Activity
                       // startActivity(new Intent(getApplication(), ActivityLogin.class));
                        break;

                    case R.id.radioButtonSQLite:
                        Toast.makeText(getApplicationContext(), "SQLite Local - Por Fazer", Toast.LENGTH_SHORT).show();
                        break;

                    default:
                        Toast.makeText(getApplicationContext(), "ERRO", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
